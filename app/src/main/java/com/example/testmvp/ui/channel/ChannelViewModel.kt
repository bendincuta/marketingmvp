package com.example.testmvp.ui.channel

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.testmvp.Models.DataModel
import com.example.testmvp.Models.Channels
import com.example.testmvp.getJsonDataFromAsset
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class ChannelViewModel(application: Application) : AndroidViewModel(application) {

    private var listItems: ArrayList<DataModel>? = null

    private val _text = MutableLiveData<String>().apply {
        value = "No targeting specifics has been selected"
    }

    fun _list() = MutableLiveData<ArrayList<DataModel>>().apply {
        if (value.isNullOrEmpty()){
            val jsonFileString = getJsonDataFromAsset(getApplication<Application>().applicationContext, "marketing.json")

            Log.i("data", jsonFileString)

            val gson = Gson()
            val listChannelType = object : TypeToken<List<Channels>>() {}.type

            var channels: List<Channels> = gson.fromJson(jsonFileString, listChannelType)

            listItems = ArrayList()

            for (i in 0 until channels.size) {
                if (sList != null) {
                    for (k in 0 until (sList.value?.size ?: 0)) {
                        if (sList.value!![k].checked == true) {

                            for (j in 0 until channels[i].channelName.size)
                                listItems!!.add(
                                    DataModel(
                                        channels[i].channelName.get(j),
                                        false
                                    )
                                )


                            when (sList.value!![k].name) {
                                "Location", "Age", "Sex" -> listItems

                                "Spoken languages" -> listItems!!.removeAll { it.name in setOf("Linkedin", "Google AdWords", "SEO")}
                                "Events" -> listItems!!.removeAll { it.name in setOf("Linkedin", "Google AdWords", "SEO", "Instagram")}
                                "Company","Expertise","Education" -> listItems!!.removeAll { it.name in setOf("Facebook", "Twitter", "Google AdWords", "SEO", "Instagram")}
                                "Industry" -> listItems!!.removeAll { it.name in setOf("Facebook", "Google AdWords", "Twitter", "Instagram")}
                                "Technology" -> listItems!!.removeAll { it.name in setOf("Facebook", "Linkedin", "Google AdWords", "SEO", "Instagram")}
                                "Interests" -> listItems!!.removeAll { it.name in setOf("Facebook", "Linkedin", "Google AdWords", "SEO", "Twitter")}
                                "Device","Similar products/services" -> listItems!!.removeAll { it.name in setOf("Facebook", "Linkedin", "Instagram", "SEO", "Twitter")}
                                "Keywords" -> listItems!!.removeAll { it.name in setOf("Facebook", "Linkedin", "Instagram", "Twitter")}
                                "Hyperlinks" -> listItems!!.removeAll { it.name in setOf("Facebook", "Linkedin", "Instagram", "Google AdWords", "Twitter")}
                            }

                        }

                    }
                }
            }


            value = listItems
        }
    }

    fun setsList(array: ArrayList<DataModel>) {
        sList.value = array
        cList.value = _list().value
    }

    val text: LiveData<String> = _text

    val cList: MutableLiveData<ArrayList<DataModel>> = _list()

    val sList = MutableLiveData<ArrayList<DataModel>>()
}