package com.example.testmvp.ui.campaigns

import android.app.Application
import android.media.FaceDetector
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.testmvp.Models.*
import com.example.testmvp.getJsonDataFromAsset
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class CampaignsViewModel(application: Application) : AndroidViewModel(application) {

    private var campaignslistItems: ArrayList<CampaignDataModel>? = null

    private var selectedChannel: DataModel? = null

    private val _text = MutableLiveData<String>().apply {
        value = "No channel has been selected"
    }

    fun _list() = MutableLiveData<ArrayList<CampaignDataModel>>().apply {
        if (value.isNullOrEmpty()){
            val jsonFileString = getJsonDataFromAsset(getApplication<Application>().applicationContext, "marketing.json")

            Log.i("data", jsonFileString)

            val gson = Gson()
            val listCampaignType = object : TypeToken<List<Campaign>>() {}.type

            var campaigns: List<Campaign> = gson.fromJson(jsonFileString, listCampaignType)

            campaignslistItems = ArrayList()
            var previouslySelectedChannel : String? = selectedChannel?.name
            if(!previouslySelectedChannel.isNullOrEmpty()) {
                var selectedOption = campaigns[0].campaign.forEachIndexed { index, campaignItems ->
                    var campaignIndex = index
                    var type = campaignItems.getCampaignListItem(previouslySelectedChannel)
                    if (type != null && campaignItems.getCampaignItem(index) == previouslySelectedChannel){
                        var priceType =
                            campaignItems.getCampaignListItem(previouslySelectedChannel).get(0)

                        when (priceType){
                            is Facebook -> priceType.price.forEachIndexed { index, marketingOptions ->
                                campaignslistItems!!.add(
                                    CampaignDataModel(
                                        campaignItems.getCampaignItem(campaignIndex),
                                        marketingOptions.getPrice(index).toString(),
                                        marketingOptions[index],
                                        false
                                    )
                                )
                            }
                            is GoogleAdWord -> priceType.price.forEachIndexed { index, marketingOptions ->
                                campaignslistItems!!.add(
                                    CampaignDataModel(
                                        campaignItems.getCampaignItem(campaignIndex),
                                        marketingOptions.getPrice(index).toString(),
                                        marketingOptions[index],
                                        false
                                    )
                                )
                            }
                            is Linkedin -> priceType.price.forEachIndexed { index, marketingOptions ->
                                campaignslistItems!!.add(
                                    CampaignDataModel(
                                        campaignItems.getCampaignItem(campaignIndex),
                                        marketingOptions.getPrice(index).toString(),
                                        marketingOptions[index],
                                        false
                                    )
                                )
                            }
                            is SEO -> priceType.price.forEachIndexed { index, marketingOptions ->
                                campaignslistItems!!.add(
                                    CampaignDataModel(
                                        campaignItems.getCampaignItem(campaignIndex),
                                        marketingOptions.getPrice(index).toString(),
                                        marketingOptions[index],
                                        false
                                    )
                                )
                            }
                            is Twitter -> priceType.price.forEachIndexed { index, marketingOptions ->
                                campaignslistItems!!.add(
                                    CampaignDataModel(
                                        campaignItems.getCampaignItem(campaignIndex),
                                        marketingOptions.getPrice(index).toString(),
                                        marketingOptions[index],
                                        false
                                    )
                                )
                            }
                            is Instagram -> priceType.price.forEachIndexed { index, marketingOptions ->
                                campaignslistItems!!.add(
                                    CampaignDataModel(
                                        campaignItems.getCampaignItem(campaignIndex),
                                        marketingOptions.getPrice(index).toString(),
                                        marketingOptions[index],
                                        false
                                    )
                                )
                            }
                        }

                    }
                }
            }

            value = campaignslistItems
        }
    }

    fun setsList(selectedChannel: DataModel) {
        this.selectedChannel = selectedChannel
        list.value = _list().value
    }

    fun getPriceList (priceType : Any?){

    }

    val text: LiveData<String> = _text

    val list: MutableLiveData<ArrayList<CampaignDataModel>> = _list()
}