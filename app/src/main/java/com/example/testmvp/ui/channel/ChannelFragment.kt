package com.example.testmvp.ui.channel

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ListView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.testmvp.*
import com.example.testmvp.Models.DataModel
import com.example.testmvp.ui.specifics.SpecificsViewModel
import com.example.testmvp.ui.campaigns.CampaignsViewModel

class ChannelFragment : Fragment() {
    private lateinit var listView: ListView
    private lateinit var adapter: CustomAdapter
    private lateinit var specificsViewModel: SpecificsViewModel
    private lateinit var channelViewModel: ChannelViewModel
    private lateinit var campaignsViewModel: CampaignsViewModel
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        specificsViewModel =
            ViewModelProviders.of(requireActivity()).get(SpecificsViewModel::class.java)
        channelViewModel=
            ViewModelProviders.of(requireActivity()).get(ChannelViewModel::class.java)
        campaignsViewModel=
            ViewModelProviders.of(requireActivity()).get(CampaignsViewModel::class.java)

        val root = inflater.inflate(R.layout.fragment_channels, container, false)
        val textView: TextView = root.findViewById(R.id.text_channel)

        channelViewModel.text.observe(requireActivity(), Observer {
            textView.text = it
        })

        listView = root.findViewById<ListView>(R.id.channels_list_view)
        lateinit var list: ArrayList<DataModel>
        specificsViewModel.list.observe(requireActivity(), Observer{
            list = it
        })

        lateinit var cList: ArrayList<DataModel>
        channelViewModel.cList.observe(requireActivity(), Observer{
            cList = it
        })

        if (list.any({ i -> i.checked == true })) {
            for (i in 0 until list.size) {
                if (list.get(i).checked == true) {
                    adapter =
                        CustomAdapter(cList, requireActivity())
                    listView.adapter = adapter

                    listView.onItemClickListener =
                        AdapterView.OnItemClickListener { _, _, position, _ ->
                            val dataModel: DataModel = channelViewModel.cList.value!![position] as DataModel
                            dataModel.checked = !dataModel.checked
                            campaignsViewModel.setsList(channelViewModel.cList.value!![position])
                            adapter.notifyDataSetChanged()
                        }
                }
            }
        }
        else{
            val textView: TextView = root.findViewById(R.id.text_channel)
            channelViewModel.text.observe(requireActivity(), Observer {
                textView.text = it
            })
            textView.visibility = TextView.VISIBLE
        }



        return root
    }
}