package com.example.testmvp.ui.specifics

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.testmvp.Models.DataModel
import com.example.testmvp.Models.Specifics
import com.example.testmvp.getJsonDataFromAsset
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class SpecificsViewModel(application: Application) : AndroidViewModel(application) {

    private var listItems: ArrayList<DataModel>? = null

    private val _text = MutableLiveData<String>().apply {
        value = "This is home Fragment"
    }

    fun _list() = MutableLiveData<ArrayList<DataModel>>().apply {
        if (value.isNullOrEmpty()){
            val jsonFileString = getJsonDataFromAsset(getApplication<Application>().applicationContext, "marketing.json")

            Log.i("data", jsonFileString)

            val gson = Gson()
            val listSpecificsType = object : TypeToken<List<Specifics>>() {}.type

            var specifics: List<Specifics> = gson.fromJson(jsonFileString, listSpecificsType)

            listItems = ArrayList<DataModel>()

            for (i in 0 until specifics.size) {

                for (j in 0 until specifics[i].specifics.size)
                    listItems!!.add(
                        DataModel(
                            specifics[i].specifics.get(j),
                            false
                        )
                    )
            }

            value = listItems
        }
    }

    val text: LiveData<String> = _text

    val list: MutableLiveData<ArrayList<DataModel>> = _list()


}