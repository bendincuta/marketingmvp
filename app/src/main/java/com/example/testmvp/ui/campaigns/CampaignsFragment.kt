package com.example.testmvp.ui.campaigns

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.testmvp.CampaignCustomAdapter
import com.example.testmvp.Models.CampaignDataModel
import com.example.testmvp.R
import com.example.testmvp.ui.channel.ChannelViewModel

class CampaignsFragment : Fragment() {

    private lateinit var campaignsViewModel: CampaignsViewModel
    private lateinit var channelViewModel: ChannelViewModel
    private lateinit var listView: ListView
    var mainAdapter: CampaignCustomAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        campaignsViewModel =
            ViewModelProviders.of(requireActivity()).get(CampaignsViewModel::class.java)
        channelViewModel=
            ViewModelProviders.of(requireActivity()).get(ChannelViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_campaigns, container, false)


        listView = root.findViewById(R.id.campaigns_list_view)

        lateinit var cList: ArrayList<CampaignDataModel>
        channelViewModel.cList.observe(requireActivity(), Observer{
            //cList = it
        })

        mainAdapter = CampaignCustomAdapter(campaignsViewModel.list.value!!, requireActivity())
        listView.adapter = mainAdapter

        if(campaignsViewModel.list.value.isNullOrEmpty()){
            val textView: TextView = root.findViewById(R.id.text_campaigns)
            campaignsViewModel.text.observe(requireActivity(), Observer {
                textView.text = it
            })
        }

        return root
    }
}