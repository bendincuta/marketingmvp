package com.example.testmvp.ui.specifics

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.example.testmvp.*
import com.example.testmvp.Models.DataModel
import com.example.testmvp.ui.channel.ChannelViewModel

class SpecificsFragment : Fragment() {

    private lateinit var listView: ListView
    var mainAdapter: CustomAdapter? = null
    lateinit var specificsViewModel: SpecificsViewModel
    private lateinit var channelViewModel: ChannelViewModel
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        specificsViewModel =
        ViewModelProviders.of(requireActivity()).get(SpecificsViewModel::class.java)

        channelViewModel=
            ViewModelProviders.of(requireActivity()).get(ChannelViewModel::class.java)

        val root = inflater.inflate(R.layout.fragment_specifics, container, false)

        listView = root.findViewById<ListView>(R.id.specifics_list_view)

        mainAdapter = CustomAdapter(specificsViewModel.list.value!!, requireActivity())
        listView.adapter = mainAdapter

        listView.setOnItemClickListener{ parent, view, position, id ->
            val dataModel: DataModel = specificsViewModel.list.value!![position]
            dataModel.checked = !dataModel.checked
            val array: ArrayList<DataModel> = specificsViewModel.list.value!!
            specificsViewModel.list.postValue(array)
            channelViewModel.setsList(array)
            mainAdapter?.notifyDataSetChanged()
        }

        return root
    }
}