package com.example.testmvp

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.CheckBox
import android.widget.TextView
import com.example.testmvp.Models.CampaignDataModel
import com.example.testmvp.Models.DataModel

class CampaignCustomAdapter(private val dataSet: ArrayList<*>, mContext: Context) :
    ArrayAdapter<Any?>(mContext, R.layout.row_item, dataSet) {
    private class ViewHolder {
        lateinit var price: TextView
        lateinit var optimisations: TextView
        lateinit var campaignSetup: TextView
        lateinit var install: TextView
        lateinit var emailSupport: TextView
        lateinit var monthlyReports: TextView
        lateinit var conversionMonitoring: TextView
        lateinit var landing: TextView
        lateinit var phoneSupport: TextView
        lateinit var remarketing: TextView
        lateinit var monitoring: TextView

        lateinit var checkBox: CheckBox
    }

    override fun getCount(): Int {
        return dataSet.size
    }

    override fun getItem(position: Int): CampaignDataModel {
        return dataSet[position] as CampaignDataModel
    }

    override fun getView(
        position: Int,
        convertView: View?,
        parent: ViewGroup
    ): View {
        var convertView = convertView
        val viewHolder: ViewHolder
        val result: View
        if (convertView == null) {
            viewHolder = ViewHolder()
            convertView =
                LayoutInflater.from(parent.context).inflate(R.layout.campaign_row_item, parent, false)
            viewHolder.price =
                convertView.findViewById(R.id.priceName)
            viewHolder.optimisations =
                convertView.findViewById(R.id.optimisationName)
            viewHolder.campaignSetup =
                convertView.findViewById(R.id.campaignName)
            viewHolder.install =
                convertView.findViewById(R.id.install)
            viewHolder.emailSupport =
                convertView.findViewById(R.id.emailName)
            viewHolder.monthlyReports =
                convertView.findViewById(R.id.monthlyName)
            viewHolder.conversionMonitoring =
                convertView.findViewById(R.id.conversionMonitoring)
            viewHolder.landing =
                convertView.findViewById(R.id.landing)
            viewHolder.phoneSupport =
                convertView.findViewById(R.id.phoneSupport)
            viewHolder.remarketing =
                convertView.findViewById(R.id.remarketing)
            viewHolder.monitoring =
                convertView.findViewById(R.id.monitoring)
            viewHolder.checkBox =
                convertView.findViewById(R.id.checkBox)
            result = convertView
            convertView.tag = viewHolder
        } else {
            viewHolder = convertView.tag as ViewHolder
            result = convertView
        }
        val item: CampaignDataModel = getItem(position)
        viewHolder.price.text = item.price + " EUR / month"

        for(i in 0 until item.marketingOptions!!.size){
            when (item.marketingOptions!![i]){
                "admin and optimizing" -> {
                    viewHolder.optimisations.text = item.marketingOptions!![i].toString()
                    viewHolder.optimisations.visibility = View.VISIBLE}
                "campaign setup" -> {
                    viewHolder.campaignSetup.text = item.marketingOptions!![i].toString()
                    viewHolder.campaignSetup.visibility = View.VISIBLE
                }
                "email support" -> {
                    viewHolder.emailSupport.text = item.marketingOptions!![i].toString()
                    viewHolder.emailSupport.visibility = View.VISIBLE
                }
                "monthly reports" -> {
                    viewHolder.monthlyReports.text = item.marketingOptions!![i].toString()
                    viewHolder.monthlyReports.visibility = View.VISIBLE
                }
                "install" -> {
                    viewHolder.install.text = item.marketingOptions!![i].toString()
                    viewHolder.install.visibility = View.VISIBLE
                }
                "conversion monitoring" -> {
                    viewHolder.conversionMonitoring.text = item.marketingOptions!![i].toString()
                    viewHolder.conversionMonitoring.visibility = View.VISIBLE
                }
                "landing page" -> {
                    viewHolder.landing.text = item.marketingOptions!![i].toString()
                    viewHolder.landing.visibility = View.VISIBLE
                }
                "phone support" -> {
                    viewHolder.phoneSupport.text = item.marketingOptions!![i].toString()
                    viewHolder.phoneSupport.visibility = View.VISIBLE
                }
                "remarketing" -> {
                    viewHolder.remarketing.text = item.marketingOptions!![i].toString()
                    viewHolder.remarketing.visibility = View.VISIBLE
                }
                "monitoring" -> {
                    viewHolder.monitoring.text = item.marketingOptions!![i].toString()
                    viewHolder.monitoring.visibility = View.VISIBLE
                }
            }
        }

        viewHolder.checkBox.isChecked = item.checked
        viewHolder.checkBox.setFocusable(false);
        viewHolder.checkBox.setFocusableInTouchMode(false);
        return result
    }
}