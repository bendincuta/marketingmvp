package com.example.testmvp.Models

data class PriceLinkedin(
    val `150`: List<Any>,
    val `180`: List<Any>,
    val `90`: List<Any>,
    val `60`: List<Any>
){
    operator fun get(index: Int): List<Any>? {
        when(index){
            0 -> return `150`
            1 -> return `180`
            2 -> return `90`
            else -> return `60`
        }
    }

    fun getPrice(priceIndex: Int): Int {
        when(priceIndex){
            0 -> return 150
            1 -> return 180
            2 -> return 90
            else -> return 60
        }
    }
}