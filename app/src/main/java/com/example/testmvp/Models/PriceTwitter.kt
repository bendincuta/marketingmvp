package com.example.testmvp.Models

data class PriceTwitter(
    val `170`: List<Any>,
    val `250`: List<Any>,
    val `120`: List<Any>,
    val `80`: List<Any>
){
    operator fun get(index: Int): List<Any>? {
        when(index){
            0 -> return `170`
            1 -> return `250`
            2 -> return `120`
            else -> return `80`
        }
    }

    fun getPrice(priceIndex: Int): Int {
        when(priceIndex){
            0 -> return 170
            1 -> return 250
            2 -> return 120
            else -> return 80
        }
    }
}