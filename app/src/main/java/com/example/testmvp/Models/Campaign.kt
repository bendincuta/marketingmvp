package com.example.testmvp.Models

import android.media.FaceDetector

data class Campaign(
    val campaign: List<CampaignItems>
)

data class CampaignItems(
    val Facebook: List<Facebook>,
    val GoogleAdWords: List<GoogleAdWord>,
    val Instagram: List<Instagram>,
    val Linkedin: List<Linkedin>,
    val SEO: List<SEO>,
    val Twitter: List<Twitter>
){
    fun getCampaignItem(index: Int): String {
        when(index){
            0 -> return "Facebook"
            1 -> return "Google Adwords"
            2 -> return "Instagram"
            3 -> return "Linkedin"
            4 -> return "SEO"
            else -> return "Twitter"
        }
    }

    fun getCampaignListItem(index: String) : List<*>{
        when(index){
            "Facebook" -> return Facebook
            "GoogleAdWord" -> return GoogleAdWords
            "Instagram" -> return Instagram
            "Linkedin" -> return Linkedin
            "SEO" -> return SEO
            else -> return Twitter
        }
    }
}



data class Facebook(
    val price: List<PricesFacebook>
)

data class Instagram(
    val price: List<PriceInstagram>
)

data class GoogleAdWord(
    val price: List<PriceGoogle>
)

data class Linkedin(
    val price: List<PriceLinkedin>
)

data class SEO(
    val price: List<PriceSeo>
)

data class Twitter(
    val price: List<PriceTwitter>
)