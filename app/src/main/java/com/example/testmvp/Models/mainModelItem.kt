package com.example.testmvp.Models

data class mainModelItem(
    val campaign: List<Campaign>,
    val channelName: List<String>,
    val specifics: List<String>
)