package com.example.testmvp.Models

data class PriceGoogle(
    val `150`: List<Any>,
    val `230`: List<Any>,
    val `90`: List<Any>
)
{
    operator fun get(index: Int): List<Any>? {
        when(index){
            0 -> return `150`
            1 -> return `230`
            else -> return `90`
        }
    }

    fun getPrice(priceIndex: Int): Int {
        when(priceIndex){
            0 -> return 150
            1 -> return 230
            else -> return 90
        }
    }
}