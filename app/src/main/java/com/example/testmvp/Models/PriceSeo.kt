package com.example.testmvp.Models

data class PriceSeo(
    val `270`: List<Any>,
    val `320`: List<Any>,
    val `180`: List<Any>,
    val `110`: List<Any>
){
    operator fun get(index: Int): List<Any>? {
        when(index){
            0 -> return `270`
            1 -> return `320`
            2 -> return `180`
            else -> return `110`
        }
    }

    fun getPrice(priceIndex: Int): Int {
        when(priceIndex){
            0 -> return 270
            1 -> return 320
            2 -> return 180
            else -> return 110
        }
    }
}