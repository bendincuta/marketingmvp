package com.example.testmvp.Models

data class PriceInstagram(
    val `180`: List<Any>,
    val `270`: List<Any>,
    val `130`: List<Any>,
    val `80`: List<Any>
){
    operator fun get(index: Int): List<Any>? {
        when(index){
            0 -> return `180`
            1 -> return `270`
            2 -> return `130`
            else -> return `80`
        }
    }

    fun getPrice(priceIndex: Int): Int {
        when(priceIndex){
            0 -> return 180
            1 -> return 270
            2 -> return 130
            else -> return 80
        }
    }
}