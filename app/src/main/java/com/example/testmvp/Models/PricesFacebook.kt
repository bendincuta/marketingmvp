package com.example.testmvp.Models

data class PricesFacebook(
    val `100`: List<Any>,
    val `140`: List<Any>,
    val `200`: List<Any>,
    val `75`: List<Any>
) {
    operator fun get(index: Int): List<Any>? {
        when(index){
            0 -> return `140`
            1 -> return `200`
            2 -> return `100`
            else -> return `75`
        }
    }

    fun getPrice(priceIndex: Int): Int {
        when(priceIndex){
            0 -> return 140
            1 -> return 200
            2 -> return 100
            else -> return 75
        }
    }
}